package com.neuvoo.pojo;

public class ParamsPojo {
    private String isoRegion;

    public ParamsPojo() {

    }

    public ParamsPojo(String isoRegion) {
        this.isoRegion = isoRegion;
    }

    public String getIsoRegion() {
        return isoRegion;
    }

    public void setIsoRegion(String isoRegion) {
        this.isoRegion = isoRegion;
    }

}
