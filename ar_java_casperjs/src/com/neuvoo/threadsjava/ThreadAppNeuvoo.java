package com.neuvoo.threadsjava;

import com.neuvoo.bean.ExecuteCommand;

public class ThreadAppNeuvoo {
	public static void main(String args[]){
        String command = "casperjs src/com/neuvoo/resources/tax_ar_v1.js";
//        String command = "casperjs src/com/neuvoo/resources/tax_ar_v2.js";
        ExecuteCommand.callCommand(command);
    }
}