package com.neuvoo.bean;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ExecuteCommand {
    public static String callCommand(String command) {

        StringBuffer output = new StringBuffer();

        Process p;
        try {
            System.out.println(command);
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine())!= null) {
                System.out.println(line + "\n");
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }
}
