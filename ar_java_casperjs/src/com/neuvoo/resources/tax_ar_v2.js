/**
 * @file Extract taxes from 'http://www.taxformcalculator.com/annual.html'.
 * @author Farid Ayaach
 * @edited Jesus Fajardo
 * @version 1.4
 */

/*************************************************************************************/
/* INITIALIZATION ********************************************************************/
/*************************************************************************************/

// environment
var fs     = require('fs');
var utils  = require('utils');
var casper = require('casper').create({
    pageSettings: {
        loadImages:  false,     // The WebPage instance used by Casper will
        loadPlugins: false,     // use these settings
        userAgent:   'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
    },
    viewportSize: {
        width:       1020,
        height:      600
    },
    logLevel:        "error",   // Only "error" level messages will be logged
    verbose:         false,     // log messages will be printed out to the console
    retryTimeout:    1000,
    waitTimeout:     30000,
});

// general
var urlname        = 'https://especialess3.lanacion.com.ar/18/12/calculadora-ganancias-2019/index.html';
var filename       = 'src/com/neuvoo/resources/json/tax_ar.json';
var regions        = null;
var labels         = null;
var values         = null;

var salaries       = [
//"250000"

//                        "1",      "20",     "50",     "70",     "100",    "200",    "400",    "600",    "800",    "900",
//                        "1000",   "2000",   "3000",   "4000",   "5000",   "6000",   "7000",   "8000",   "9000",   "10000",
//                        "11000",  "12000",  "13000",  "14000",  "15000",  "16000",  "17000",  "18000",  "19000",  "20000",
//                        "25000",  "30000",  "35000",  "40000",  "45000",  "50000",  "55000",  "60000",  "65000",  "70000",
//                        "75000",  "80000",  "85000",  "90000",  "95000",  "100000", "110000", "120000", "130000", "140000",
//                        "150000", "160000", "170000", "180000", "190000", "200000", "300000", "400000", "500000", "600000",
                        "700000", "800000", "900000", "1000000"

                    ];

// resources
var resourcesToWaitForOnLoad = [
    'jquery.min.js'
];

// wait-for selectors
var selectorsToWaitForOnLoad = [
    'form',
];
var selectorsToWaitForOnChange = [
    '#pn-result',
];

// other selectors
var formSelector          = 'form input#sueldoBruto';
var submitButtonSelector  = 'form button#calcular';
var labelsSelector        = '#pn-result .form-group:nth-child(1) label';
var valuesSelector        = '#impuestoAnual';

// excluded words
var wordsBlacklist = [
];

// if there are any, get values from CLI
if(casper.cli.get('filename')){
    filename = casper.cli.get('filename');
}


/*************************************************************************************/
/* PROCESSING ************************************************************************/
/*************************************************************************************/

/* STEP 1: Wait for all resources and selectors to load properly to get the regions list. */
casper.start(urlname, function() {

    logToConsole('Connected to: "' + urlname + '"', 'INFO');

    // wait for the specified resources
    console.log('Waiting for specified resources to load...');
    this.each(resourcesToWaitForOnLoad, function(self, item, index) {
        self.waitForResource(item,
            function then() {},
            function onTimeout() {
                this.echo('- Resource "' + item + '" did not load in timely fashion. Skipping it...');
            }
        );
    });

    // wait for the specified selectors
    logToConsole('Waiting for specified selectors to load...');
    this.each(selectorsToWaitForOnLoad, function(self, item, index) {
        self.waitForSelector(item,
            function then() {},
            function onTimeout() {
                self.die('- Selector "' + item + '" did not load in time. Can\'t continue.',1);
            }
        );
    });

    // get regions list
    regions = ["Argentina"]//this.evaluate(getFromDOM, regionOptionsSelector);
    // regions = regions.slice(0, 1);//Testing using less regions

});

/* STEP 2: Manage CLI input. */
casper.then(function() {

    // check if must filter regions
    if( casper.cli.get('onlyfor') ) {

        var rx = casper.cli.get('onlyfor');

        regions = Array.prototype.filter.call(
            regions,
            function(rs) {
                return clean(rs).search(rx) >= 0;
            }
        );

    }

    // check if must filter regions
    if( casper.cli.get('skipregion') ) {

        var rx = casper.cli.get('skipregion');

        regions = Array.prototype.filter.call(
            regions,
            function(rs) {
                return clean(rs).search(rx) < 0;
            }
        );

    }

    // check if only must get regions
    if( casper.cli.get('dumpregions') ) {
        regions = Array.prototype.map.call(regions, clean);

        utils.dump(regions);
        this.exit(0);
    }

});

/* STEP 3: Extract taxes for every region and salary. */
casper.then(function() {

    var limit = regions.length * salaries.length;

    this.echo('Processing ' + regions.length + ' regions.');

    // initialize (and truncate) the output file
    fs.write(filename, '[', 'w');

    // for each region...
    this.each(regions, function(self, region, s) {

        // for each salary...
        this.each(salaries, function(self, salary, i) {

            var index = (s + 1) * (i + 1);

            // fill and submit 'state' and 'salary' form
            this.then(function() {

//                this.fill(formSelector, {
//                    'form input#sueldoBruto'  : salary.toString(),
//                }, false);  // auto-submit is not working for this URL

                this.sendKeys("input#sueldoBruto", salary.toString());
                this.click(submitButtonSelector);

            });

            // wait for the results...
            this.each(selectorsToWaitForOnChange, function(self, item, index) {
                self.waitForSelectorTextChange(item,
                    function then() {

                        this.echo('- Processing ' + region + ', salary = ' + salary);

                        // get labels and values
                        labels = this.evaluate(getFromDOM, labelsSelector);
                        values = this.evaluate(getFromDOM, valuesSelector);
                        console.log(JSON.stringify(labels ,null ,4));//Before labels array clean out
                        console.log(JSON.stringify(values ,null ,4));//Before values array clean out

                        // discard table headings
//                        labels.shift();
//                        values.shift();


                        // cleanout labels and values
//                        removeExcluded(labels, values);

                        // save the data to the output file
                        var data = formatAsJSON(region.replace(/US Territories: /gi, ''), salary, labels, values);

                        if (index < limit) {
                            data += ',';
                        }

                        fs.write(filename, data, 'a');

                    },
                    function onTimeout() {
                        this.echo('- Skipping ' + region + '; salary = ' + salary + ' because of timeout...');
                    }
                );
            });

        });

    });

});

/*************************************************************************************/
/* CLEANING **************************************************************************/
/*************************************************************************************/

casper.run(function() {

    fs.write(filename, ']', 'a');
    json_data = fs.read(filename);
    json_data = json_data.replace("}},]", "}}]");
    fs.write(filename, json_data, 'w');
    this.echo('Done!', 'INFO');
    this.exit(0);

});

/*************************************************************************************/
/* FUNCTION DEFINITIONS **************************************************************/
/*************************************************************************************/

/**
 * Builds a minified JSON formatted string based on the parameters.
 * The return string would be like:
 * @example
 * // {
 * //     "id": "NULL",
 * //     "salary": "0",
 * //     "country": "CA",
 * //     "year": "2018",
 * //     "region": "Alberta",
 * //     "data": {
 * //                 "CPP": "0.00",
 * //                 "EI": "0.00",
 * //                 ...
 * //             }
 * // }
 *
 * Notice that is assumed that 'labels.length' === 'values.length'.
 * Empty labels[i] will be omitted.
 *
 * @param   {string}   region - The state name
 * @param   {string}   salary - The current salary
 * @param   {string[]} labels - Labels to the JSON parameters
 * @param   {string[]} values - JSON Values
 * @returns {string}            The minified formatted JSON string
 */
function formatAsJSON(region, salary, labels, values) {
    var output  = '{';
    output += '"frequency":"Yearly",';
    output += '"salary":"' + salary + '",';
    output += '"country":"AR",';
    output += '"year":"2019",';
    output += '"region":"' + region + '",';
    output += '"data":{';

    //Delete the duplicates
    var array_dupl = search_duplicate(labels);
    // logToConsole("\n\n" + array_dupl + "\n\n");
    for(i = 0; i < array_dupl.length; i++){
        index_to_delete = array_dupl[i];
        labels.splice(index_to_delete, 1);
        values.splice(index_to_delete, 1);
    }

    for (var i = 0; i < labels.length; i++) {

        var label = labels[i].replace(region, '').trim();
        var value = values[i].trim();

        if (label.length > 0) {
            output += '"' + label + '":"' + value + '",';
        }

    }

    // remove the last comma and close brackets
    output  = output.slice(0,-1);
    output += '}}';


    return output;

}

/**
 * Check if a word is in the excluded list, without using case-sensitivity.
 *
 * @param   {string}   str - Any string
 * @returns {boolean}        'str' without newlines, tabs, double-spaces, ...
 */
function clean(str) {
    return str.replace(/\n|\t|\$/g, '')
        .replace(/\ +/g, ' ')
        .replace(/US Territories: /gi, '');
}

/**
 * Check if a word is in the excluded list, without using case-sensitivity.
 *
 * @param   {string}   labels     - Any string array
 * @param   {string}   values     - Any string array
 * @returns {boolean}               If a 'labels[i]' matches any 'wordsBlacklist[i]', returns 'false'.
 */
function removeExcluded(labels, values) {

    var patterns = wordsBlacklist;

    for (var i = 0; i < patterns.length; i++) {

        patterns[i] = patterns[i].replace("(", "").replace(")", "");
        var regex = new RegExp(patterns[i], 'i');
        for (var j = 0; j < labels.length; j++) {
            labels[j] = labels[j].replace("(", "").replace(")", "");
            if (regex.test(labels[j]) || regex.test(values[j])) {
                labels.splice(j, 1);
                values.splice(j, 1);
                // logToConsole(patterns[i] + " - " + labels[j] + " - " + values[j]);
            }else{
                //Formatting values
                values[j] = values[j].replace(/(\,)/g, "");
                values[j] = values[j].replace(/(\$)/g, "");
                values[j] = values[j].trim();
                values[j] = parseFloat(values[j]).toFixed(2);
                // logToConsole(patterns[i] + " - " + labels[j] + " - " + values[j]);
            }
        }

    }

    return true;

}

/**
 * Queries DOM for a given selector and gets it's innerHTML.
 *
 * @param   {string} selector - The selector to query.
 * @returns {Array}             An array of HTML DOM elements
 */
function getFromDOM(selector) {

    var elements = document.querySelectorAll(selector);

    return Array.prototype.map.call(elements, function(e) {
        return e.innerHTML;
    });

}

/**
 * Logs a message to console
 *
 * @param {string} message - The message to log.
 */
function logToConsole(message) {
    console.log(message);
}

/**
 * Search duplicates array and return the index of everyone
 *
 * @param {Array} array_basic - Array with information
 * @returns {Array} An array of index of duplicate elements
 */
function search_duplicate(array_basic) {
    var index_array = [];
    Array.prototype.contains = function(elem) {
        for (var i in this) {
            if (this[i] == elem) return true;
        }
        return false;
    }

    for(i = 0; i < array_basic.length; i++){//Loop for get the item to search
        item_i = array_basic[i];
        for(j = i+1; j < array_basic.length; j++){//Loop for get the item to compare
            item_j = array_basic[j];
            if(item_i == item_j){
                exist = index_array.contains(j)//Validate if the index not exist in the final index array
                if(!exist){
                    index_array.push(j);
                }
            }
        }
    }

    return index_array;
}